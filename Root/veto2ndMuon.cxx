#include "MyWlnuSelection/veto2ndMuon.h"

namespace top {
	// Select only events with one Highpt muon
	bool veto2ndMuon::apply(const top::Event& event) const {
		unsigned nMuons = event.m_muons.size();
		if (nMuons != 1) return false;
		else if (!event.m_muons.at(0)->passesHighPtCuts()){
			return false;
		}
		return true;

	}

	//For the cutflow and terminal output
	std::string veto2ndMuon::name() const {
		return "VETO2NDMUON";
	}
}

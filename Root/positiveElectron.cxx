#include "MyWlnuSelection/positiveElectron.h"

namespace top {
	// Select only events with positive electron
	bool positiveElectron::apply(const top::Event& event) const {
		unsigned nElectrons = event.m_electrons.size();
		if (nElectrons != 1) return false;
		if (event.m_electrons.at(0)->charge() <= 0) return false;
		return true;

	}
	//for (auto & mu : event.m_muons) 

	//For the cutflow and terminal output
	std::string positiveElectron::name() const {
		return "POSELEC";
	}
}

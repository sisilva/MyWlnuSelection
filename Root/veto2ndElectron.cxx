#include "MyWlnuSelection/veto2ndElectron.h"

namespace top {
	// Select only events with one TightLH electron
	bool veto2ndElectron::apply(const top::Event& event) const {
		unsigned nElectrons = event.m_electrons.size();
		if (nElectrons != 1) return false;
		/*else {
			try {
				if (!event.m_electrons.at(0)->auxdataConst<int>("DFCommonElectronsLHTight") != 1) 
					return false;
			}
			catch (const std::exception& e) {
				if (!event.m_electrons.at(0)->auxdataConst<char>("DFCommonElectronsLHTight") != 1)
					return false;
			}
		}*/
		return true;

	}

	//For the cutflow and terminal output
	std::string veto2ndElectron::name() const {
		return "VETO2NDELECTRON";
	}
}

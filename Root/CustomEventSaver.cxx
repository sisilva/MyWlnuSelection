#include "MyWlnuSelection/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"

#include "xAODTruth/TruthParticle.h"

#include <TRandom3.h>


namespace top{
	///-- Constrcutor --///
	CustomEventSaver::CustomEventSaver() :
		//m_randomNumber(0.),
		//m_someOtherVariable(0.)
		m_LPXKfactor(0.),
		m_bornMass(0.),
		m_MT(0.),
		m_Menu(0.),
		m_mu(0.),
		m_nPVTX(0)

	{  
		m_p_kfactorTool = new LPXKfactorTool("LPXKfactorTool");
	}

	///-- initialize - done once at the start of a job before the loop over events --///
	void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
	{
		///-- Let the base class do all the hard work --///
		///-- It will setup TTrees for each systematic with a standard set of variables --///
		top::EventSaverFlatNtuple::initialize(config, file, extraBranches);

		///-- Loop over the systematic TTrees and add the custom variables --///
		for (auto systematicTree : treeManagers()) {
			//systematicTree->makeOutputVariable(m_randomNumber, "randomNumber");
			//systematicTree->makeOutputVariable(m_someOtherVariable,"someOtherVariable");
			systematicTree->makeOutputVariable(m_LPXKfactor, "LPXKfactor");
			systematicTree->makeOutputVariable(m_bornMass, "WbornMass");
			systematicTree->makeOutputVariable(m_MT, "truthMT");
			systematicTree->makeOutputVariable(m_Menu, "Menu");
			systematicTree->makeOutputVariable(m_mu, "muEvtInfo");
			systematicTree->makeOutputVariable(m_nPVTX, "nPVTX");
		}
		if (!m_p_kfactorTool->setProperty("isMC15",true).isSuccess()) {
			std::cerr << "Error initializing LPXKfactorTool. Exiting...\n";
			std::exit(1);
		}
		if (!m_p_kfactorTool->initialize().isSuccess()) {
			std::cerr << "Error initializing LPXKfactorTool. Exiting...\n";
			std::exit(1);
		}
	}

	///-- saveEvent - run for every systematic and every event --///
	void CustomEventSaver::saveEvent(const top::Event& event) 
	{
		///-- set our variables to zero --///
		//m_randomNumber = 0.;
		//m_someOtherVariable = 0.;
		//Get the number of interactions and primary vertices
		m_mu = event.m_info->averageInteractionsPerCrossing();
		m_nPVTX = 0;
		const DataVector< xAOD::Vertex > *PrimaryVertices = event.m_primaryVertices;
		m_nPVTX = PrimaryVertices->size();
		///-- Fill them - you should probably do something more complicated --///
		//TRandom3 random( event.m_info->eventNumber() );
		//m_randomNumber = random.Rndm();    
		//m_someOtherVariable = 42;
		m_LPXKfactor = 0.;
		if (event.m_info->eventType(xAOD::EventInfo::IS_SIMULATION)) {
			// Get LPXKfactor
			if (!m_p_kfactorTool->getKFactor(m_LPXKfactor).isSuccess()) {
				std::cerr << "Error getting LPXKfactor. Exiting...\n";
				std::exit(1);
			}
			// Get W born mass and Wenu mass
			for (const auto *truthP : *(event.m_truth)) {
				if (truthP->isW()) {
					m_bornMass = truthP->p4().M();
					break;
				}

			}
			// Get M_enu
			double p1 = 0;
			double p2 = 0;
			double phi1 = 0;
			double phi2 = 0;
			xAOD::IParticle::FourMom_t p4l;
			xAOD::IParticle::FourMom_t p4nu;
			for (const auto *truthP : *(event.m_truth)) {
				if ((truthP->isElectron()) && (truthP->status() == 3)) {
					p1 = truthP->pt();
					phi1 = truthP->phi();
					p4l = truthP->p4();
				}
				if ((truthP->isNeutrino()) && (truthP->status() == 3)) {
					p2 = truthP->pt();
					phi2 = truthP->phi();
					p4nu = truthP->p4();
				}
				if ((p1 != 0) && (p2 != 0)) {
					m_MT = 2 * p1 * p2 * (1 - cos(phi1 - phi2));
					m_MT = sqrt(m_MT);
					xAOD::IParticle::FourMom_t p4W = p4l + p4nu;
					m_Menu = p4W.M();
					break;
				}
			}

		}
		///-- Let the base class do all the hard work --///
		top::EventSaverFlatNtuple::saveEvent(event);
	}

}

#include "MyWlnuSelection/CustomMuon.h"

#include "TopEvent/EventTools.h"

namespace top {

	CustomMuon::CustomMuon(const double ptcut, IsolationBase* isolation) :
		m_ptcut(ptcut),
		m_muonSelectionTool("CP::MuonSelectionTool"),
		m_muonSelectionToolLoose("CP::MuonSelectionToolLoose"),
		m_isolation(isolation)
	{
		top::check( m_muonSelectionTool.retrieve() , "Failed to retrieve muonSelectionTool" );
		top::check( m_muonSelectionToolLoose.retrieve() , "Failed to retrieve muonSelectionToolLoose" );
	}

	bool CustomMuon::passSelection(const xAOD::Muon& mu) const 

	{
		///-- combined muon --///
		if (mu.auxdata<unsigned short>("muonType") != xAOD::Muon::MuonType::Combined)
			return false;

		if (mu.pt() < m_ptcut)
			return false; 

		///-- https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC15 --///
		if (m_muonSelectionTool->passedHighPtCuts(mu) &&
			m_muonSelectionTool->passedIDCuts(mu)) {
			xAOD::Muon *muAux = (xAOD::Muon *) &mu; //TODO maybe there is a safier way of do this
			muAux->setPassesHighPtCuts(true);
		}
		else if (!m_muonSelectionTool->accept(mu))
			return false;
		
		///-- d0 significance and z0 cuts --///
		if (!passTTVACut(mu)) return false;

		//isolation, if m_isolation != nullptr
		if (m_isolation && !m_isolation->passSelection(mu))
			return false;

		return true;
	}

	bool CustomMuon::passSelectionLoose(const xAOD::Muon& mu) const
	{
		///-- combined muon --///
		if (mu.auxdata<unsigned short>("muonType") != xAOD::Muon::MuonType::Combined)
			return false;

		if (mu.pt() < m_ptcut)
			return false; 

		///-- https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesMC15 --///
		if (!m_muonSelectionToolLoose->accept(mu))
			return false;

		///-- d0 significance and z0 cuts --///
		if (!passTTVACut(mu)) return false;

		///-- exclude muon if 1.01<|eta|<1.1 --///
		if ((fabs(mu.eta()) > 1.01) && (fabs(mu.eta()) < 1.1))
			return false;
			
		//isolation, if m_isolation != nullptr
		if (m_isolation && !m_isolation->passSelectionLoose(mu))
			return false;    

		return true;
	}

	bool CustomMuon::passTTVACut(const xAOD::Muon& mu) const
	{
		///-- d0 significance and z0 cuts --///
		if (!mu.isAvailable<float>("d0sig")) {
			std::cout << "d0 significance not found for muon. "
				<< "Maybe no primary vertex? Won't accept." << std::endl;
			return false;
		}
		float d0sig = mu.auxdataConst<float>("d0sig");
		if (std::fabs(d0sig) >= 3) return false;
		if(!mu.isAvailable<float>("delta_z0_sintheta")) {
			std::cout << "delta z0*sin(theta) not found for muon. "
				<< "Maybe no primary vertex? Won't accept." << std::endl;
			return false;
		}
		float delta_z0_sintheta = mu.auxdataConst<float>("delta_z0_sintheta");
		if (std::abs(delta_z0_sintheta) >= 0.5) return false;
		return true;
	}

	void CustomMuon::print(std::ostream& os) const {
		os << "CustomMuon\n"
			<< "    * pT > " << m_ptcut << "\n"
			<< "    * Everything else from muon tool - fill this in?\n";

		if (!m_isolation)
			os << "    * No isolation requirement\n";
		else
			m_isolation->print(os);
	}
}

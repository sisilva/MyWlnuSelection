#include "MyWlnuSelection/negativeMuon.h"

namespace top {
	// Select only events with positive muon
	bool negativeMuon::apply(const top::Event& event) const {
		unsigned nMuons = event.m_muons.size();
		if (nMuons != 1) return false;
		if (event.m_muons.at(0)->charge() >= 0) return false;
		return true;

	}
	//for (auto & mu : event.m_muons) 

	//For the cutflow and terminal output
	std::string negativeMuon::name() const {
		return "NEGMUON";
	}
}

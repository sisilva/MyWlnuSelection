#include "MyWlnuSelection/CustomObjectLoader.h"
#include "MyWlnuSelection/MyWlnuSelectionLoader.h"
#include "MyWlnuSelection/CustomEventSaver.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//for loading the object selection at run time
#pragma link C++ class top::CustomObjectLoader+;
#pragma link C++ class top::MyWlnuSelectionLoader+;
#pragma link C++ class top::CustomEventSaver+;
#endif

#include "MyWlnuSelection/negativeElectron.h"

namespace top {
	// Select only events with positive electron
	bool negativeElectron::apply(const top::Event& event) const {
		unsigned nElectrons = event.m_electrons.size();
		if (nElectrons != 1) return false;
		if (event.m_electrons.at(0)->charge() >= 0) return false;
		return true;

	}
	//for (auto & mu : event.m_muons) 

	//For the cutflow and terminal output
	std::string negativeElectron::name() const {
		return "NEGELEC";
	}
}

#include "MyWlnuSelection/MyWlnuSelectionLoader.h"

#include "MyWlnuSelection/veto2ndMuon.h"
#include "MyWlnuSelection/veto2ndElectron.h"
#include "MyWlnuSelection/positiveMuon.h"
#include "MyWlnuSelection/negativeMuon.h"
#include "MyWlnuSelection/positiveElectron.h"
#include "MyWlnuSelection/negativeElectron.h"
#include "MyWlnuSelection/M120.h"

#include "TopConfiguration/TopConfig.h"

#include <iostream>
#include "TFile.h"

namespace top{
	top::EventSelectorBase* 
		MyWlnuSelectionLoader::initTool(const std::string& /*name*/,
		const std::string& line, TFile* /*outputFile*/,
		std::shared_ptr<top::TopConfig> /*config*/,EL::Worker* /*wk*/)
	{
		//get the first bit of the string and store it in toolname
		std::istringstream iss(line);
		std::string toolname;
		getline(iss, toolname, ' ');

		//any parameters?
		std::string param;
		if (line.size() > toolname.size())
			param = line.substr(toolname.size() + 1);

		if (toolname == "VETO2NDMUON")
			return new top::veto2ndMuon();
		if (toolname == "VETO2NDELECTRON")
			return new top::veto2ndElectron();
		else if (toolname == "POSMUON")
			return new top::positiveMuon();
		else if (toolname == "NEGMUON")
			return new top::negativeMuon();
		else if (toolname == "POSELEC")
			return new top::positiveElectron();
		else if (toolname == "NEGELEC")
			return new top::negativeElectron();
		else if (toolname == "M120")
			return new top::M120;
		//else if (toolname.find("OTHER_TOOL") == 0)
		//  return OtherToolThatYouInvented()    

		return nullptr;      
	}
}

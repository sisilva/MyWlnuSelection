#ifndef VETO2NDELECTRON_H
	#define VETO2NDELECTRON_H
	
	#include "TopEventSelectionTools/EventSelectorBase.h"
	#include "TopEvent/Event.h"
	
namespace top{

	/**
	 * Veto a possible second electron.
	 */
	class veto2ndElectron : public top::EventSelectorBase {
		public:
			//This function sees every event.  If you return true then the event passes this "cut"
			bool apply(const top::Event& event) const override;

			//For humans, something short and catchy
			std::string name() const override;
	}; 

}
	
	#endif

#ifndef NEGATIVEMUON_H
	#define NEGATIVEMUON_H
	
	#include "TopEventSelectionTools/EventSelectorBase.h"
	#include "TopEvent/Event.h"
	
namespace top{

	/**
	 * This is a tool that selects only negative muons.
	 */
	class negativeMuon : public top::EventSelectorBase {
		public:
			//This function sees every event.  If you return true then the event passes this "cut"
			bool apply(const top::Event& event) const override;

			//For humans, something short and catchy
			std::string name() const override;
	}; 

}
	
	#endif


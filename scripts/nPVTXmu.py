#/usr/bin/env python

import sys
import ROOT

if len(sys.argv) < 3:
	sys.exit("Usage: nPVTXmu.py [file list] [DATA/MC]\n")

#read files

lista = open(sys.argv[1], "r")
chain = ROOT.TChain("nominal")

while True:
	fName = lista.readline()
	fName = fName[:-1]
	if fName == "": break
	chain.Add(fName)

lista.close()

#book histograms for nPVTX
h = []

isData = (sys.argv[2] == "DATA")
if isData: hScaled = []
muList = range(45)
for mu in muList:
	hName = "nPVTXhist"+"{0:02d}".format(mu)
	h.append(ROOT.TH1F(hName, "nPVTX", 50, 0, 25))
	if isData:
		hName = "nPVTXhistsScaled"+"{0:02d}".format(mu)
		hScaled.append(ROOT.TH1F(hName, "nPVTX", 50, 0, 25))

nEntries = chain.GetEntries()
sys.stdout.write("chain has "+str(nEntries)+" events\n")
#Main loop
for evt in range(nEntries):
	chain.GetEntry(evt)
	if chain.InclusiveSelection == 1:
		index = int(chain.mu)
		if index > 44: continue
		h[index].Fill(chain.nPVTX)
		if isData:
			index = int(1.09 * chain.mu)
			hScaled[index].Fill(chain.nPVTX)

nPVTXmu = ROOT.TGraph(45)
if isData:
	nPVTXmuScaled = ROOT.TGraph(45)

for mu in muList:
	print "mu= ", mu, " nPVTX= ", h[mu].GetMean()
	nPVTXmu.SetPoint(mu, float(mu), h[mu].GetMean())
	if isData:
		nPVTXmuScaled.SetPoint(mu, float(mu), hScaled[mu].GetMean())
	
outFile = ROOT.TFile("nPVTXmu.root", "recreate")
nPVTXmu.Write("nPVTXmu")

if isData:
	nPVTXmuScaled.Write("nPVTXmuScaled")

outFile.Write()
outFile.Close()

#!/usr/bin/env python
"""
Simple script to get final efficiency and
(final sum of weights)/(initial sum of weights)
"""
import ROOT
import sys

if len(sys.argv) < 3:
	sys.exit("Usage: efficiency [file] [selection name]\n")

initialW = 0
finalW = 0
unskim = 0
skim = 0
hNameW = sys.argv[-1] + "/cutflow_mc_pu"
hNameEvt = sys.argv[-1] + "/cutflow_mc"

for k in range(1, len(sys.argv) - 1):
	fName = sys.argv[k]
	#print "Adding file: ", fName
	f = ROOT.TFile(fName, "r")
	# Get eff * (final sum of weights) / (initial sum of weights)
	hW = f.Get(hNameW)
	initialW = initialW + hW.GetBinContent(1)
	finalW = finalW + hW.GetBinContent(hW.GetNbinsX())
	# Get skimming normalization factor
	hEvt = f.Get(hNameEvt)
	tree = f.Get("sumWeights")
	tree.GetEntry()
	unskim = unskim + tree.totalEventsWeighted
	skim = skim + hEvt.GetBinContent(1)
	f.Close()

skimNorm = float(skim) / unskim
#eff = skimNorm
eff = float(finalW) / unskim
sys.stdout.write("Final efficiency ")
sys.stdout.write("{0:6.5e}".format(eff)+"\n")

#!/usr/bin/env python
"""
Simple script to get initial sum of weights
"""

import ROOT
import sys

if len(sys.argv) < 2:
	sys.exit("Usage: initialEvtMC.py [files]\n")

unskim = 0

for fName in sys.argv[1:]:
	f = ROOT.TFile(fName, "r")
	sys.stderr.write("Opening "+fName+"\n")
	tree = f.Get("sumWeights")
	tree.GetEntry()
	unskim = unskim + tree.totalEventsWeighted
	f.Close()

sys.stdout.write("Initial events*MC ")
sys.stdout.write("{0:6.5e}".format(unskim)+"\n")

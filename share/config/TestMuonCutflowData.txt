LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libMyWlnuSelection

#--> GOOD RUNS LIST
GRLDir RootCoreBin/data/MyWlnuSelection/GRL
GRLFile data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml
#--> PILEUP REWEIGHTING
PRWConfigFiles RootCoreBin/data/MyWlnuSelection/PRW_lumicalc/ttbarCutflow.prw.root
PRWLumiCalcFiles RootCoreBin/data/MyWlnuSelection/PRW_lumicalc/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-005.root
#PRWConfigFiles TopCorrections/PRW.410000.mc15c.r7725_r7676.root
#PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root GoodRunsLists/data16_13TeV/20160916/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root
#PRWDefaultChannel 410000

#--> FAST SIMULATION
IsAFII False

#--> ELECTRON CONFIGURATION
ElectronCollectionName Electrons
ElectronID MediumLH
ElectronIDLoose MediumLH
ElectronIsolation Loose
ElectronPt 20000
ElectronLarCrack True

#--> MUON CONFIGURATION
MuonCollectionName Muons
MuonIsolation Gradient
#MuonIsolation LooseTrackOnly
MuonIsolationLoose LooseTrackOnly
MuonPt 20000
MuonQuality Medium
MuonQualityLoose Medium
MuonTriggerSF HLT_mu50

#--> JET CONFIGURATION
JetCollectionName AntiKt4EMTopoJets
LargeJetCollectionName None
LargeJetSubstructure None
JetPt 20000
JetEta 2.4
JVTinMETCalculation True

#--> TAU CONFIGURATION
TauCollectionName None

#--> PHOTON CONFIGURATION
PhotonCollectionName None

#-->MET CONFIGURATION
METCollectionName MET_Reference_AntiKt4EMTopo

#--> TRUTH CONFIGURATION
TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthJets

#--> FRAMEWORK CONFIGURATION
ObjectSelectionName top::ObjectLoaderStandardCuts
#ObjectSelectionName top::CustomObjectLoader
#OutputFormat top::CustomEventSaver
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename outputTestCutflowData.root

NEvents -1000

SELECTION MuonSelection

INITIAL
GRL
GOODCALO
PRIVTX
TRIGDEC HLT_mu50 HLT_mu20_iloose_L1MU15
#TRIGMATCH
MU_N 55000 == 1
MU_N 20000 == 1
#POSMUON
#VETO2NDMUON
EL_N 20000 == 0
JETCLEAN LooseBad
MET > 55000
MWT > 110000
SAVE
#PRINT
